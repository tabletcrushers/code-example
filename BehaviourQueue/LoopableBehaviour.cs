using UnityEngine;

namespace DDDCasino
{
	public class LoopableBehaviour : QueuedBehaviour, ILoopable, IResetable, IInitable
	{
		[Space(10)]
		[SerializeField] private int loopLength = 1;
		[SerializeField] private int loopsNumber = 1;

		public int LoopLength => loopLength;

		public int CurrentLoopNumber { get; set; }

		public virtual void DDDReset()
		{
			CurrentLoopNumber = loopsNumber;
		}

		public void Init()
		{
			CurrentLoopNumber = loopsNumber;
		}
	}
}