namespace DDDCasino
{
	public interface ILoopable : IResetable
	{
		int LoopLength { get; }
		int CurrentLoopNumber { get; set; }
	}
}
