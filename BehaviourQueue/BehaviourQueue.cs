﻿namespace DDDCasino
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class BehaviourQueue : MonoBehaviour
    {
        public QueuedBehaviour CurrentBehaviour { get { return queuedBehaviours[currentBehaviourId]; } }

        private List<IResetable> resetableBehaviours;
        private List<IInitable> initableBehaviours;
        private List<QueuedBehaviour> queuedBehaviours;
        private Action completeCallback;
        private int currentBehaviourId;

        public void OnComplete(Action callback)
        {
            completeCallback = callback;
        }

        public void ForceStop()
        {
            enabled = false;
            CurrentBehaviour.ForceStop();
            resetableBehaviours.ForEach(b => b.DDDReset());
            StopAllCoroutines();
        }

        public void StartQueue()
        {
            enabled = true;
            currentBehaviourId = 0;
            CurrentBehaviour.enabled = true;
            TryToSetParallelAbility(CurrentBehaviour, true);

            initableBehaviours.ForEach(b => b.Init());

            StartCoroutine(LyfeCycle());
        }

		private void OnDestroy()
		{
            StopAllCoroutines();
		}

		private void Awake()
        {
            queuedBehaviours = new List<QueuedBehaviour>(GetComponents<QueuedBehaviour>());
            resetableBehaviours = new List<IResetable>(GetComponents<IResetable>());
            initableBehaviours = new List<IInitable>(GetComponents<IInitable>());
        }

        private void Comlete()
        {
            completeCallback?.Invoke();
            enabled = false;

            resetableBehaviours.ForEach(b => b.DDDReset());
        }

        private IEnumerator LyfeCycle()
        {
            while (CurrentBehaviour.enabled || IsParallelEnabled(CurrentBehaviour)) yield return null;

            if (CurrentBehaviour is ILoopable loopAble && loopAble.CurrentLoopNumber != 0)
            {
                currentBehaviourId -= loopAble.LoopLength;
                if (loopAble.CurrentLoopNumber > 0) loopAble.CurrentLoopNumber--;

                CurrentBehaviour.enabled = true;
                TryToSetParallelAbility(CurrentBehaviour, true);

                yield return null;

                StopAllCoroutines();
                StartCoroutine(LyfeCycle());
            }
            else
            {
                if (currentBehaviourId < queuedBehaviours.Count - 1)
                {
                    currentBehaviourId++;
                    CurrentBehaviour.enabled = true;
                    TryToSetParallelAbility(CurrentBehaviour, true);
                }
                else { enabled = false; }

                if (enabled)
                {
                    yield return null;

                    StopAllCoroutines();
                    StartCoroutine(LyfeCycle());
                }
                else
                {
                    Comlete();
                }
            }
        }

        private bool IsParallelEnabled(QueuedBehaviour behaviour)
        {
            if (behaviour.parallelBehaviours == null) return false;

            foreach (var parallelLogic in behaviour.parallelBehaviours)
            {
                if (parallelLogic.enabled) return true;
            }

            return false;
        }

        private void TryToSetParallelAbility(QueuedBehaviour behaviour, bool isEnabled)
        {
            if (behaviour.parallelBehaviours == null || behaviour.parallelBehaviours.Count == 0) return;

            foreach (var parallelBehaviour in behaviour.parallelBehaviours) parallelBehaviour.enabled = isEnabled;
        }
    }
}
