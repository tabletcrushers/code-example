using System;
using UnityEngine;

namespace DDDCasino
{
    public class ParallelBehaviour : MonoBehaviour
    {
        public bool IsRootResponsible { get => isRootResponsible; }

        [HideInInspector] public Action stopRoot;
        [SerializeField] bool isRootResponsible;

        protected virtual void Awake()
        {
            if (enabled) throw new Exception($"{this} - Must be disabled in Inspector");
        }

        protected virtual void OnDisable()
        {
            StopAllCoroutines();
            stopRoot?.Invoke();
        }
    }
}