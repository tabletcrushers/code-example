using System.Collections.Generic;
using UnityEngine;

namespace DDDCasino
{
    public class QueuedBehaviour : MonoBehaviour
    {
        [HideInInspector]
        public List<ParallelBehaviour> parallelBehaviours = null;

        [SerializeField] private Transform parallelLogicContainer;
        [SerializeField] private bool parallelsResponsible;

        public void ForceStop()
        {
            enabled = false;

            if (parallelLogicContainer == null) return;

            foreach(var parallelLogic in parallelBehaviours)
            {
                parallelLogic.enabled = false;
            }
        }

        virtual protected void OnDisable()
		{
            StopAllCoroutines();

            if (parallelsResponsible) ForceStop();
		}

		virtual protected void Awake()
        {
            if (enabled) throw new System.Exception($"{this} - Must be disabled in Inspector");

            if (parallelLogicContainer == null) return;

            parallelBehaviours = new List<ParallelBehaviour>(parallelLogicContainer.GetComponents<ParallelBehaviour>());

            foreach (var parallelLogic in parallelBehaviours)
            {
                if (parallelLogic.IsRootResponsible) parallelLogic.stopRoot = ForceStop; 
            }
        }
    }
}