using System.Collections.Generic;
using System;
using System.Reflection;
using UnityEngine;

namespace DDDCasino
{
    public class DataRepository
    {
        public static DataRepository Instance
        {
            get
            {
                if (instance == null) instance = new DataRepository();

                return instance;
            }

            private set { }
        }

        private static DataRepository instance;
        private Dictionary<string, object> storage = new Dictionary<string, object>();
        private Dictionary<string, Action<object>> observers = new Dictionary<string, Action<object>>();

        public void AddObserver(string id, Action<object> callback)
        {
            if (observers.TryGetValue(id, out Action<object> cb))
            {
                observers[id] += callback;
            }
            else
			{
                observers[id] = callback;
            }
        }

        public void RemoveObserver(string id, Action<object> callback)
        {
            if (observers.TryGetValue(id, out Action<object> cb))
            {
                observers[id] -= callback;
            }
        }

        public void Add(string id, object data)
        {
            if (storage.ContainsKey(id))
            {
                storage.Remove(id);
            }

            storage.Add(id, data);
            
            if (observers.TryGetValue(id, out Action<object> callback))
            {
                callback?.Invoke(data);
            }
        }

        public T Get<T>(string id)
        {
            if (!storage.TryGetValue(id, out object data)) return default(T);

            return (T)data;
        }

        public void Remove(string id)
        {
            storage.Remove(id);
        }

        public void Clear()
        {
            storage.Clear();
        }

        public void ClearByLabels(params Type[] args)
		{
            foreach(Type t in args)
			{
                FieldInfo[] fields = t.GetFields(BindingFlags.Static | BindingFlags.Public);

                foreach (FieldInfo fi in fields)
                {
                    storage.Remove(fi.GetValue(null).ToString());
                }
            }
        }

        public void ClearAllButApp()
		{
            var appKeys = new List<string>();
            var cleanStorage = new Dictionary<string, object>();

            FieldInfo[] fields = typeof(AppRepositoryLabels).GetFields(BindingFlags.Static | BindingFlags.Public);

            foreach (FieldInfo fi in fields)
            {
                appKeys.Add(fi.GetValue(null).ToString());
            }

            foreach (KeyValuePair<string, object> entry in storage)
            {
                if (appKeys.Contains(entry.Key))
                {
                    cleanStorage.Add(entry.Key, entry.Value);
                }
            }

            storage = cleanStorage;
		}
    }
}