using UnityEngine;

namespace DDDCasino
{
    [DefaultExecutionOrder(-7)]
    public class Host : MonoBehaviour, IHost
    {
        public virtual IWebRequestManager WebRequestManager { get; protected set; }
        public virtual ObjectPool ObjectPool { get; protected set; }
        public SoundManager SoundManager { get; protected set; }

        protected virtual void Awake()
        {
            ObjectPool = GetComponent<ObjectPool>();
            SoundManager = GetComponent<SoundManager>();

            WebRequestManager = GetComponent<IWebRequestManager>();
            if (WebRequestManager == null) WebRequestManager = transform.root.GetComponent<ApplicationHost>().WebRequestManager;
        }

        protected virtual void OnDestroy()
		{
            StopAllCoroutines();
		}
    }
}