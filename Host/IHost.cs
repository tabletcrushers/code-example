namespace DDDCasino
{
	public interface IHost
	{
		SoundManager SoundManager { get; }
		ObjectPool ObjectPool { get; }
		IWebRequestManager WebRequestManager { get; }
	}
}
