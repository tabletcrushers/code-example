using System;
using System.Collections.Generic;
using UnityEngine;

namespace DDDCasino
{
    public class Notificator
    {
        private static Notificator _instance;
        private Dictionary<string, Action<object[]>> _eventsMap;

        public Notificator()
        {
            _eventsMap = new Dictionary<string, Action<object[]>>();
        }

        public static Notificator Instance { 
            get 
            {
                if (_instance == null) _instance = new Notificator();

                return _instance;
            }

            private set { }
        }

        public void AddListener(string eventName, Action<object[]> listener)
        {
            if (!_eventsMap.ContainsKey(eventName)) _eventsMap.Add(eventName, listener);
            else _eventsMap[eventName] += listener;
        }

        public void RemoveListener(string eventName, Action<object[]> listener)
        {
            if (_eventsMap == null || !_eventsMap.ContainsKey(eventName))
            {
                DisplayNotAddedWarning(eventName);

                return;
            }

            _eventsMap[eventName] -= listener;
        }

        public void Notify(string eventName, params object[] arguments)
        {
            if (!_eventsMap.TryGetValue(eventName, out Action<object[]> listeners)) DisplayNotAddedWarning(eventName);

            listeners?.Invoke(arguments);
        }

        private void DisplayNotAddedWarning(string eventName)
        {
            Debug.LogWarning($"WARNING: {eventName} has not been added yet");
        }
    }
}
