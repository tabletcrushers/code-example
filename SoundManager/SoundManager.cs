using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DDDCasino
{
	public class SoundManager : MonoBehaviour
	{
        [SerializeField] private List<AudioClip> clips;
        [SerializeField] private int sourcesQuantity = 3;

        private List<AudioSource> stoppedSources = new List<AudioSource>();
        private Dictionary<string, AudioClip> clipsMap = new Dictionary<string, AudioClip>();
        private Dictionary<string, float> musicClipsVolumeMap = new Dictionary<string, float>();
        private Dictionary<string, AudioSource> playingSources = new Dictionary<string, AudioSource>();

        private void Awake()
        {
            clips.ForEach(c => clipsMap.Add(c.name, c));

            for (var i = 0; i < sourcesQuantity; i++)
            {
                stoppedSources.Add(gameObject.AddComponent<AudioSource>());
            }

            Notificator.Instance.AddListener(NotificationLabels.MusicOnOff, OnMusicOnOff);
            Notificator.Instance.AddListener(NotificationLabels.SoundEffectsOnOff, OnSoundEffectsOnOff);

            StartCoroutine(CheckSoundsCompletion());
        }

		private void OnDisable()
		{
            Notificator.Instance.RemoveListener(NotificationLabels.MusicOnOff, OnMusicOnOff);
            Notificator.Instance.AddListener(NotificationLabels.SoundEffectsOnOff, OnSoundEffectsOnOff);
        }

        private void OnMusicOnOff(object[] args)
        {
            if (!(bool)args[0])
            {
                foreach (string name in musicClipsVolumeMap.Keys)
                {
                    playingSources[name].Stop();
                }
            }
            else
            {
                foreach (string name in musicClipsVolumeMap.Keys.ToList())
                {
                    Play(name, true, true, 0, musicClipsVolumeMap[name], 1f);
                }
            }
        }

        private void OnSoundEffectsOnOff(object[] args)
        {
            if (!(bool)args[0])
            {
                foreach (string name in playingSources.Keys)
                {
                    if (!musicClipsVolumeMap.ContainsKey(name))
					{
                        playingSources[name].Stop();
                    }
                }
            }
        }

        public void Stop(string name)
        {
            if (musicClipsVolumeMap.ContainsKey(name))
            {
                musicClipsVolumeMap.Remove(name);
            }

            if (!playingSources.TryGetValue(name, out AudioSource source))
            {
                return;
            }

            source.DOKill();
            source.Stop();
        }

        public void Fade(string name, float targetVolume, float duration = 0f, Action completeCallback = null, bool stopAfterFade = true)
        {
            if (stopAfterFade && clipsMap.TryGetValue(name, out AudioClip clip))
			{
                musicClipsVolumeMap.Remove(name);
            }

            if (!playingSources.TryGetValue(name, out AudioSource source))
            {
                return;
            }

            source.DOKill();

            var tween = source.DOFade(targetVolume, duration).SetEase(Ease.Linear);
            tween.OnKill(ProcessStop);
            tween.OnComplete(ProcessStop);

            void ProcessStop()
			{
                if (stopAfterFade)
                {
                    Stop(name);

                    return;
                }

                source.volume = targetVolume;

                completeCallback?.Invoke();
            }
        }

        public void Play(string name, bool music = false, bool loop = false, float startVolume = 1f, float endVolume = 1f, float fadeDuration = 0f)
        {
            if (!clipsMap.TryGetValue(name, out AudioClip clip))
            {
                throw new System.Exception($"{name} has not been added");
            }

            if (music)
            {
                musicClipsVolumeMap[name] = endVolume;

                if (!Convert.ToBoolean(PlayerPrefs.GetInt(PlayerPrefsLabels.SettingsMusicOn)))
				{
                    return;
				}
            }
            else if (!Convert.ToBoolean(PlayerPrefs.GetInt(PlayerPrefsLabels.SettingsSoundEffectsOn)))
			{
                return;
			}

            if (playingSources.TryGetValue(name, out AudioSource source))
            {
                source.Stop();
                PlaySound();

                return;
            }

            if (stoppedSources.Count > 0)
            {
                source = stoppedSources[0];
                stoppedSources.RemoveAt(0);
            }
            else source = gameObject.AddComponent<AudioSource>();

            source.clip = clip;
            source.loop = loop;
            playingSources.Add(name, source);
            PlaySound();

            void PlaySound()
			{
                source.DOKill();
                source.Play();
                source.volume = startVolume;
                source.loop = loop;

                if (fadeDuration > 0)
                {
                    Fade(name, endVolume, fadeDuration, null, false);
                }
            }
        }

        public bool IsPlaing(string name)
        {
            if (playingSources.TryGetValue(name, out AudioSource source))
            {
                return true;
            } else
            {
                return false;
            }
        }

        protected virtual void OnDestroy()
		{
            StopAllCoroutines();
        }

        private IEnumerator CheckSoundsCompletion()
        {
            var i = 0;

            while (true)
            {
                if (playingSources.Count > 0)
                {
                    if (i == playingSources.Count)
                    {
                        i = 0;
                    }

                    try
					{
                        var entrie = playingSources.ElementAt(i);
                        if (!entrie.Value.isPlaying)
                        {
                            playingSources.Remove(entrie.Key);
                            stoppedSources.Add(entrie.Value);
                        }

                        i++;
                    }
                    catch
					{

					}
                    
                }

                yield return new WaitForSeconds(.1f);
            }
        }
    }
}