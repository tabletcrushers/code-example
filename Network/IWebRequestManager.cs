using System;
using System.Collections;
using UnityEngine.Networking;

namespace DDDCasino
{
	public interface IWebRequestManager
	{
        public UnityWebRequest BuildGetRequest(string url);
        public UnityWebRequest BuildPostRequest(string url, string body);
        public UnityWebRequest SetHeader(UnityWebRequest request, string headerName, string headerValue);
        public void SendRequest<T>(UnityWebRequest request, Action<T> callback) where T : CommonNetworkResponseDTO;
    }
}