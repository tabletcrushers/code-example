using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using System.Runtime.CompilerServices;

namespace DDDCasino
{
    public class WebRequestManager : MonoBehaviour, IWebRequestManager
    {
        [SerializeField] string refreshTokenLocalUri = "auth/refresh_token";

        protected string AccessToken => PlayerPrefs.GetString(PlayerPrefsLabels.AccessToken);
        protected string RefreshToken => PlayerPrefs.GetString(PlayerPrefsLabels.RefreshToken);
        protected string ServerUrl => DataRepository.Instance.Get<MainConfig>(AppRepositoryLabels.MainConfig).applicationConfig.serverUrl;

        public UnityWebRequest BuildGetRequest(string url)
        {
            var request = UnityWebRequest.Get(url);
            SetHeader(request, "accept", "application/json");
            SetHeader(request, "Content-Type", "application/json");

            return request;
        }

        public UnityWebRequest BuildPostRequest(string url, string body)
        {
            var request = new UnityWebRequest(url, "POST");
            byte[] bodyRaw = Encoding.UTF8.GetBytes(body);
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();
            SetHeader(request, "accept", "application/json");
            SetHeader(request, "Content-Type", "application/json");

            return request;
        }

        public UnityWebRequest SetHeader(UnityWebRequest request, string headerName, string headerValue)
        {
            request.SetRequestHeader(headerName, headerValue);
            return request;
        }

        public void SendRequest<T>(UnityWebRequest request, Action<T> callback) where T : CommonNetworkResponseDTO
        {
            StartCoroutine(SendWebRequest(request, callback));
        }

        protected virtual void OnDestroy()
		{
            StopAllCoroutines();
		}

        protected void TryRefreshToken(Action callback)
        {
            var url = ServerUrl + refreshTokenLocalUri;
            var body = new RereshRequestTokenDTO();
            body.refreshToken = PlayerPrefs.GetString(PlayerPrefsLabels.RefreshToken);
            var request = BuildPostRequest(url, JsonConvert.SerializeObject(body));

            SetHeader(request, "authorization", RefreshToken);
            SendRequest<TokensResponseDTO>(request, (response) => {
                if (response.success == true)
                {
                    ParseRefreshResponse(response);
                    callback.Invoke();
                }
                else
                {
                    // send notification auth fail for return app to login
                    PlayerPrefs.DeleteKey(PlayerPrefsLabels.AccessToken);
                    PlayerPrefs.DeleteKey(PlayerPrefsLabels.RefreshToken);

                    Notificator.Instance.Notify(AppNotificationLabels.AuthorizationFailed);
                }
            });
        }

        protected void ParseRefreshResponse(TokensResponseDTO response)
        {
            PlayerPrefs.SetString(PlayerPrefsLabels.AccessToken, response.data.accessToken);
            PlayerPrefs.SetString(PlayerPrefsLabels.RefreshToken, response.data.refreshToken);
        }

        private IEnumerator SendWebRequest<T>(UnityWebRequest request, Action<T> callback) where T : CommonNetworkResponseDTO
        {
            yield return request.SendWebRequest();

            T response = default;

            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogWarning(this.GetType() + ": Network Error: " + request.error);
                    Debug.LogWarning(request.downloadHandler.text);

                    var errorMessage = "Network error";

                    try
					{
                        response = JsonConvert.DeserializeObject<T>(request.downloadHandler.text);
                    }
                    catch
					{
                        Notificator.Instance.Notify(AppNotificationLabels.AppError, errorMessage);
                        break;
                    }
                    

                    if (response.message == "jwt_expired")
					{
                        callback?.Invoke(response);
                    }
                    else
					{
                        DataRepository.Instance.Add(AppRepositoryLabels.NextStateName, AppStateNameAliases.InitState);

                        switch (response.message)
                        {
                            case "user_already_exists":
                                errorMessage = "User already exists!";
                                break;

                            case "can_not_verify_refresh_token":
                                PlayerPrefs.DeleteKey(PlayerPrefsLabels.AccessToken);
                                PlayerPrefs.DeleteKey(PlayerPrefsLabels.RefreshToken);
                                DataRepository.Instance.Add(AppRepositoryLabels.NextStateName, AppStateNameAliases.AuthenticationState);
                                errorMessage = "Session expaired<br>Log in once again";
                                break;
                            case "user_not_found":
                                PlayerPrefs.DeleteKey(PlayerPrefsLabels.AccessToken);
                                PlayerPrefs.DeleteKey(PlayerPrefsLabels.RefreshToken);
                                DataRepository.Instance.Add(AppRepositoryLabels.NextStateName, AppStateNameAliases.AuthenticationState);
                                errorMessage = "Email or password is wrong<br>Register";
                                break;

                            case "not_enough_balance":
                                DataRepository.Instance.Add(AppRepositoryLabels.NextStateName, AppStateNameAliases.ShopState);
                                errorMessage = "Insufficient funds";
                                break;
                            case "insufficient_player_level":
                                errorMessage = "Insufficient level";
                                break;

                            default:
                                errorMessage = "Network error";
                                PlayerPrefs.DeleteKey(PlayerPrefsLabels.AccessToken);
                                PlayerPrefs.DeleteKey(PlayerPrefsLabels.RefreshToken);
                                Debug.Log(PlayerPrefs.GetString(PlayerPrefsLabels.AccessToken));
                                break;
                        }

                        Notificator.Instance.Notify(AppNotificationLabels.AppError, errorMessage);
                    }

                    break;

                case UnityWebRequest.Result.Success:
                    Debug.Log(">>> : " + this.GetType());
                    Debug.Log(request.downloadHandler.text);
                    response = JsonConvert.DeserializeObject<T>(request.downloadHandler.text);
                    callback?.Invoke(response);

                    break;
            }
        }

		private void OnDisable()
		{
            StopAllCoroutines();
		}
	}
}