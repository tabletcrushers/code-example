namespace DDDCasino
{
	public class Popup : BasePopup
	{
		private int hideTweensCopmleted = 100000;
		private int showTweensCopmleted = 100000;

		private ITweenEffect[] tweenEffects;

	    protected virtual void Awake()
		{
			tweenEffects = transform.GetComponentsInChildren<ITweenEffect>();

			foreach (var effect in tweenEffects)
			{
				effect.OnDestinationReached = () =>
				{
					
					showTweensCopmleted++;
					if (showTweensCopmleted == tweenEffects.Length)
					{
						OnPopupShown?.Invoke();
					}
				};

				effect.OnOriginReached = () =>
				{
					hideTweensCopmleted++;
					if (hideTweensCopmleted == tweenEffects.Length)
					{
						OnPopupHidden?.Invoke();

						gameObject.SetActive(false);

						if (destroyAfterHide)
						{
							AddressableManager.Instance.ReleaseInstance(gameObject);
						}
					}
				};
			}
		}

		public override void Hide()
		{
			Stop();

			hideTweensCopmleted = 0;

			foreach (var effect in tweenEffects)
			{
				effect.TweenToOrigin();
			}
		}

		public override void Show()
		{
			base.Show();

			Stop();

			showTweensCopmleted = 0;

			foreach (var effect in tweenEffects)
			{
				effect.TweenToDestination();
			}
		}

		virtual protected void Stop()
		{
			foreach (ITweenEffect effect in tweenEffects)
			{
				effect.Stop();
			}
		}
	}
}