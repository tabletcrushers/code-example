﻿using System;
using UnityEngine;

namespace DDDCasino
{
    [Serializable]
    public abstract class BasePopup : MonoBehaviour, IPopup
    {
        [SerializeField] protected float autocloseInterval = 0;
        [SerializeField] protected bool destroyAfterHide;

        public Action OnPopupShown { protected get; set; }
        public Action OnPopupHidden { protected get; set; }

        public abstract void Hide();

        public virtual void Show()
        {
            gameObject.SetActive(true);

            if (autocloseInterval > 0)
			{
                DOTweenUtils.Delay(() => Hide(), autocloseInterval);
            }
        }
    }
}
