using System;
namespace DDDCasino
{
	public interface IPopup : IShowHide
	{
		Action OnPopupShown { set; }
		Action OnPopupHidden { set; }
	}
}