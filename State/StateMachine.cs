using System.Collections.Generic;
using UnityEngine;

namespace DDDCasino
{
    [DefaultExecutionOrder(1)]
    public class StateMachine : MonoBehaviour
    {
        protected BehaviourQueue currentState;

        private Dictionary<BehaviourQueue, List<ITransitionFromState>>statesTransitionsMap = 
            new Dictionary<BehaviourQueue, List<ITransitionFromState>>();

        virtual protected void Awake()
        {
            foreach (Transform stateTransform in transform)
            {
                var transitions = new List<ITransitionFromState>();

                foreach (Transform transitionTransform in stateTransform)
                {
                    var transition = transitionTransform.GetComponent<ITransitionFromState>();
                    if (transition != null) transitions.Add(transition);
                }

                statesTransitionsMap.Add(stateTransform.GetComponent<BehaviourQueue>(), transitions);
            }
        }

        private void OnEnable()
        {
            currentState = transform.GetChild(0).GetComponent<BehaviourQueue>();

            if (currentState == null) throw new System.Exception("StateMachine transform can only contain states (with BehaviourQueue attached)");

            currentState.StartQueue();
        }

        private void Update()
        {
            if (currentState == null)
            {
                enabled = false;
                Debug.LogWarning(transform.name + ": StateMachine just stoped");

                return;
            }

            if (!currentState.enabled)
            {
                var transitions = statesTransitionsMap[currentState];

                foreach (var transition in transitions)
                {
                    if (transition.Evaluate())
                    {
                        SwitchState(transition);
                        transitions.ForEach((t) => t.DDDReset());

                        return;
                    }
                }
            }
        }

        private void SwitchState(ITransitionFromState transition)
        {
            currentState = transition.TargetState;
            currentState.StartQueue();
        }

        virtual protected void OnDestroy()
        {
            statesTransitionsMap.Clear();
            currentState = null;
        }
    }
}
