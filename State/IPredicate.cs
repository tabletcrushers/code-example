namespace DDDCasino
{
    public interface IPredicate : IResetable
    {
        bool Evaluate();
    }
}