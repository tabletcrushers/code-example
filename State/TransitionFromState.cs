using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DDDCasino
{
    public class TransitionFromState : MonoBehaviour, ITransitionFromState
    {
        [SerializeField] private BehaviourQueue targetState;

        [Header("Transforms with predicates attached")]
        [Space(10)]
        [SerializeField] private Transform andPredicatesContainer;
        [SerializeField] private Transform orPredicatesContainer;

        private List<IPredicate> andPredicates;
        private List<IPredicate> orPredicates;

        public BehaviourQueue TargetState => targetState;

        public bool Evaluate()
        {
            if (andPredicates == null && orPredicates == null) return true;

            var andEvaluation = andPredicates == null ? false : andPredicates.All((p) => p.Evaluate());
            var orEvaluation = orPredicates == null ? false : orPredicates.Any((p) => p.Evaluate());

            return andEvaluation || orEvaluation;
        }

        public void DDDReset()
        {
            andPredicates?.ForEach((p) => p.DDDReset());
            orPredicates?.ForEach((p) => p.DDDReset());
        }

        private void Awake()
        {
            andPredicates = andPredicatesContainer ? new List<IPredicate>(andPredicatesContainer.GetComponents<IPredicate>()) : null;
            orPredicates = orPredicatesContainer ? new List<IPredicate>(orPredicatesContainer.GetComponents<IPredicate>()) : null;
        }
    }
}