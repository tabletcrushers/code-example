using UnityEngine;

namespace DDDCasino
{
	public class HostHolderParallelFeature : ParallelBehaviour
	{
        protected IHost host;

        protected override void Awake()
        {
            base.Awake();

            host = GetComponentInParent<IHost>();
        }
    }
}
