using UnityEngine;

namespace DDDCasino
{
    public class HostHolderLoopFeature : LoopableBehaviour
    {
        protected IHost host;

        protected override void Awake()
        {
            base.Awake();

            host = GetComponentInParent<IHost>();
        }
    }
}
