using System.Collections.Generic;

namespace DDDCasino
{
    public interface ITransitionFromState : IResetable
    {
        BehaviourQueue TargetState { get; }

        bool Evaluate();
    }
}
