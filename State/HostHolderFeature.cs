namespace DDDCasino
{
    public class HostHolderFeature : QueuedBehaviour
    {
        protected IHost host;

        protected override void Awake()
        {
            base.Awake();

            host = GetComponentInParent<IHost>();
        }
    }
}
