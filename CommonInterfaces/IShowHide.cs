using System;

namespace DDDCasino
{
	public interface IShowHide
	{
		void Show();
		void Hide();
	}
}