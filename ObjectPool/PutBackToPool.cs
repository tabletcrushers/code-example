namespace DDDCasino
{
    using UnityEngine;

    public class PutBackToPool : MonoBehaviour
    {
        public bool inPool;
        public ObjectPool pool;
        public bool keepWorldPosition = true;

        public void PutBack()
        {
            pool.PutBack(gameObject, keepWorldPosition);
        }
    }
}
