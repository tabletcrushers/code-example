using System;
using System.Collections.Generic;
using UnityEngine;

namespace DDDCasino
{
    public class ObjectPool : MonoBehaviour
    {
        private Dictionary<string, GameObject> prefabsMap = new Dictionary<string, GameObject>();
        private Dictionary<string, List<GameObject>> itemsMap = new Dictionary<string, List<GameObject>>();

        [Serializable]
        public struct Objects
        {
            public int startingQuantity;
            public GameObject item;
        }
        
        [SerializeField] List<Objects> objects;

        private void Awake()
        {
            objects.ForEach((obj) => 
            {
                prefabsMap.Add(obj.item.name, obj.item);
                var list = new List<GameObject>();
                itemsMap.Add(obj.item.name, list);

                for (var i = 0; i < obj.startingQuantity; i++)
                {
                    var item = InstantiateItem(obj.item, false);
                    item.GetComponent<PutBackToPool>().inPool = true;
                    list.Add(item);
                }
            });
        }

        public GameObject Fetch(string name)
        {
            if (!itemsMap.TryGetValue(name, out List<GameObject> pool)) throw new System.Exception($"{name} has not been added into ObjectPool");

            if (pool.Count == 0) return InstantiateItem(prefabsMap[name]);

            var item = pool[0];
            item.GetComponent<PutBackToPool>().inPool = false;
            pool.RemoveAt(0);

            return item;
        }

        public void PutBack(GameObject item, bool keepWorldPosition = true)
        {
            if (!itemsMap.TryGetValue(item.name, out List<GameObject> pool))
            {
                Debug.LogWarning($"{item.name} has not been added into ObjectPool");

                return;
            }

            var backToPool = item.GetComponent<PutBackToPool>();

            if (backToPool.inPool)
            {
                Debug.LogWarning($"{item.name} This item is in pool already");

                return;
            }
           

            backToPool.inPool = true;

            if (gameObject.activeSelf)
            {
                item.transform.SetParent(transform, keepWorldPosition);
                item.SetActive(false);
            }
            else
            {
                AddressableManager.Instance.ReleaseInstance(item);
            }

            pool.Add(item);
            foreach (var resetable in item.GetComponents<IResetable>()) resetable.DDDReset();
        }

        private GameObject InstantiateItem(GameObject item, bool isActive = false)
        {
            var newItem = Instantiate(item);
            newItem.name = item.name;
            newItem.SetActive(isActive);
            newItem.AddComponent<PutBackToPool>();
            var backToPool = newItem.GetComponent<PutBackToPool>();
            backToPool.pool = this;
            backToPool.inPool = false;

            newItem.transform.parent = transform;

            return newItem;
        }
    }
}
