using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace DDDCasino
{
    public class LoadPrefab : MonoBehaviour
    {
        [SerializeField] Transform slotParent;
        [SerializeField] AssetReference assetReference;
        [SerializeField] bool needInstantiate = true;

        public UnityEvent<GameObject> onLoadComplete;
        public UnityEvent<float> onLoadProgress;

        public void Load()
        {
            AsyncOperationHandle<GameObject> handler = Addressables.LoadAssetAsync<GameObject>(assetReference);
            handler.Completed += OnLoad;
            StartCoroutine(Progress(handler));
        }

        private void OnLoad(AsyncOperationHandle<GameObject> loadedObj)
        {
            GameObject loadResult = loadedObj.Result as GameObject;

            if (needInstantiate)
            {
                GameObject game = Instantiate(loadResult);

                if (slotParent != null)
                {
                    game.transform.SetParent(slotParent);
                }

                onLoadComplete.Invoke(game);
            }
            else
            {
                onLoadComplete.Invoke(loadResult);
            }
        }

        private IEnumerator Progress(AsyncOperationHandle obj)
        {
            while (!obj.IsDone)
            {
                onLoadProgress.Invoke(obj.GetDownloadStatus().Percent);
                Debug.Log(obj.PercentComplete);
                Debug.Log(obj.GetDownloadStatus().Percent);
                Debug.Log("=>" + obj.GetDownloadStatus().DownloadedBytes);
                Debug.Log(obj.GetDownloadStatus().TotalBytes);
                if (obj.PercentComplete == 1)
                    break;
                yield return new WaitForSeconds(0.01f);
            }
        }
    }
}
