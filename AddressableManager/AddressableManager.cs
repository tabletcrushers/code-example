﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace DDDCasino
{
    public class AddressableManager
    {
        private static ApplicationHost application;

        public static AddressableManager Instance
        {
            get
            {
                if (instance == null)
                {
                    application = Object.FindObjectOfType<ApplicationHost>();
                    instance = new AddressableManager();
                }

                return instance;
            }

            private set { }
        }

        private static AddressableManager instance;

        public void Init(Action completeCallback)
		{
            application.StartCoroutine(InitAdressabless(completeCallback));
		}

        public void Clear()
		{
            Addressables.ClearResourceLocators();
        }

        public void LoadCatalog(string catalogPath, Action completeCallback)
		{
            Addressables.LoadContentCatalogAsync(catalogPath, true).Completed += loadContentCatalogHandle =>
            {
                Addressables.CheckForCatalogUpdates().Completed += operationHandle =>
                {
                    if (operationHandle.Result.Count > 0)
                    {
                        Addressables.UpdateCatalogs(operationHandle.Result).Completed += asyncOperationHandle =>
                        {
                            Debug.Log("catalog: " + asyncOperationHandle.Result);
                        };
                    }
                    else
                    {
                        Debug.Log("catalog: " + operationHandle.Result);
                    }

                    completeCallback?.Invoke();
                };
            };
        }

        #region public methods

        public void Instantiate(string assetRefKey, Transform parent = null, Action<GameObject> completeCallback = null, Action<float> progressCallback = null)
        {
            //assetRefKey += SizePrefix;

            AsyncOperationHandle<GameObject> loadHandler = Addressables.InstantiateAsync(assetRefKey, parent);
            loadHandler.Completed += (asset) => completeCallback?.Invoke(asset.Result);

            if (progressCallback != null) application.StartCoroutine(LoadingProgress(loadHandler, progressCallback));
        }

        public void ReleaseInstance(GameObject obj)
        {
            Addressables.ReleaseInstance(obj);
        }

        public void LoadAsset(string assetRefKey, Action<Object> completeCallback, Action<float> progressCallback = null)
        {
            AsyncOperationHandle<Object> loadHandler = Addressables.LoadAssetAsync<Object>(assetRefKey);
            loadHandler.Completed += (asset) => completeCallback?.Invoke(asset.Result);

            if (progressCallback != null) application.StartCoroutine(LoadingProgress(loadHandler, progressCallback));
        }

        public void GetAssetSize(string key, Action<long> callback)
        {
            Addressables.GetDownloadSizeAsync(key).Completed += (size) => callback?.Invoke(size.Result);
        }

        public void LoadAssets(string lable, Action<List<Object>> completeCallback, Action<float> progressCallback = null)
        {
            var loadedAssets = new List<Object>();
            AsyncOperationHandle<IList<Object>> loadHandler = Addressables.LoadAssetsAsync<Object>(lable, asset => loadedAssets.Add(asset));
            loadHandler.Completed += (assets) => completeCallback?.Invoke(loadedAssets);

            if (progressCallback != null) application.StartCoroutine(LoadingProgress(loadHandler, progressCallback));
        }
        #endregion

        #region private methods
        private IEnumerator InitAdressabless(Action completeCallback)
        {
            yield return Addressables.InitializeAsync();

            Addressables.CheckForCatalogUpdates().Completed += checkforupdates =>
            {
                if (checkforupdates.Result.Count > 0)
                {
                    Debug.Log("Available Update");
                }
                else
                {
                    Debug.Log("No Available Update");
                }

                completeCallback?.Invoke();
            };
        }

        private IEnumerator LoadingProgress<T>(AsyncOperationHandle<T> loadHandler, Action<float> onProgressCallback = null)
        {
            while (loadHandler.PercentComplete < 1 && !loadHandler.IsDone)
            {
                onProgressCallback.Invoke(loadHandler.PercentComplete);
                yield return null;
            }
        }
        #endregion
    }
}
